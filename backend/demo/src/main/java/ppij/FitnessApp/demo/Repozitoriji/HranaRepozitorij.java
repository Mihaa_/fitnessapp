package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Hrana;

public interface HranaRepozitorij extends JpaRepository<Hrana, Integer> {
}
