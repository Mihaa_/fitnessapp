package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;
import ppij.FitnessApp.demo.Modeli.Korisnik;
import ppij.FitnessApp.demo.Modeli.San;
import ppij.FitnessApp.demo.Repozitoriji.BodyMassIndexRepozitorij;
import ppij.FitnessApp.demo.Repozitoriji.KorisnikRepozitorij;
import ppij.FitnessApp.demo.Repozitoriji.SanRepozitorij;

import java.time.Duration;
import java.util.*;

@Service
public class KorisnikServis {

    @Autowired
    KorisnikRepozitorij korRepo;
    @Autowired
    BodyMassIndexRepozitorij bmiRepo;
    @Autowired
    SanRepozitorij sanRepo;

    public void dodajKorisnika(Korisnik korisnik){korRepo.save(korisnik); }


    public void obrisiKorisnika(Korisnik korisnik){
        korRepo.delete(korisnik);
    }

    public Korisnik dohvatiKorisnika(int idKorisnika){
        return korRepo.findById(idKorisnika).get();
    }

    public Map<String, Float> zadnjeTezine(int idKorisnika, Date datum) {
        List<BodyMassIndex> bmievi = bmiRepo.findAll();
		Collections.sort(bmievi);
        Map<String, Float> tezine = new HashMap<>();
        for (BodyMassIndex bmi : bmievi) {
            if (bmi.getIdKor() == idKorisnika && (datum.getTime() / 100000000 - bmi.getDatum().getTime() / 100000000) <= 7) {
                if(tezine.size() == 7) break;
                System.out.println(bmi.getDatum().toString() + " " + datum.getTime() + " dobije se: " + (bmi.getDatum().getTime() / 1000000000 - datum.getTime() / 1000000000));
                tezine.put(bmi.getDatum().toString().substring(5, 10), bmi.getTezina());
            }
        }
        //System.out.println(tezine);
        return tezine;
    }

    public Map<String, Float> zadnjiSnovi(int idKorisnika, Date datum) {
        List<San> snovi = sanRepo.findAll();
		Collections.sort(snovi);
        Map<String, Float> zadnjiSnovi = new HashMap<>();
        for (San san : snovi) {
            //System.out.println(san.getDatum().toString() + " " + datum.getTime() + " dobije se: " + (san.getDatum().getTime() / 1000000000 - datum.getTime() / 1000000000));
            //System.out.println((san.getDatum().getTime() / 100000000 + " " +  datum.getTime() / 100000000) + " = " + (san.getDatum().getTime() / 100000000 - datum.getTime() / 100000000));
            if (san.getIdKor() == idKorisnika && (datum.getTime() / 100000000 - san.getDatum().getTime() / 100000000) <= 7) {
                System.out.println(san.getDatum().toString());
                //if (zadnjiSnovi.size() == 7) break;
                float trajanje = Duration.between(san.getPocetak(), san.getKraj()).getSeconds()/60;
                String datumString = san.getDatum().toString().substring(5, 10);

                zadnjiSnovi.put(datumString, trajanje);
            }
        }
        //System.out.println(tezine);
        return zadnjiSnovi;
    }

    public Korisnik dohvatiKorisnikaPoEmailu(String email) {
        return korRepo.findByEmail(email);
    }
}
