package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Vjezba;

public interface VjezbaRepozitorij extends JpaRepository<Vjezba,Integer> {
}
