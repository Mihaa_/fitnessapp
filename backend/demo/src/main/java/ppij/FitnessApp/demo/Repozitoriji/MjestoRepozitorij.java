package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Mjesto;

public interface MjestoRepozitorij extends JpaRepository<Mjesto,Integer> {
}
