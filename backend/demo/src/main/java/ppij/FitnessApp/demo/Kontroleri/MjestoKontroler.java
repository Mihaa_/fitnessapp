package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Mjesto;
import ppij.FitnessApp.demo.Modeli.MjestoProsjek;
import ppij.FitnessApp.demo.Servisi.MjestoServis;

import java.util.List;

@RestController
@RequestMapping("/mjesto")
public class MjestoKontroler {

    @Autowired
    MjestoServis mjestoServis;

    @PostMapping("/dodaj")
    public void dodajMjesto(@RequestBody Mjesto mjesto){
        mjestoServis.dodajMjesto(mjesto);
    }

    @GetMapping("/dohvati/{id}")
    public Mjesto dohvatiMjesto(@PathVariable(value="id") int idMjesta){
        return mjestoServis.dohvatiMjesto(idMjesta);
    }

    @GetMapping("/dohvatiNajbolje")
    public List<MjestoProsjek> najboljihPet() {
        return mjestoServis.najboljihPet();
    }

    @GetMapping("/dohvati")
    public List<Mjesto> dohvatiSvaMjesta(){
        return mjestoServis.dohvatiSvaMjesta();
    }


}
