package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Hrana;
import ppij.FitnessApp.demo.Repozitoriji.HranaRepozitorij;

import java.util.List;

@Service
public class HranaServis {

    @Autowired
    HranaRepozitorij hranaRepo;

    public void dodajHranu(Hrana hrana){
        hranaRepo.save(hrana);
    }

    public void obrisiHranu(Hrana hrana){
        hranaRepo.delete(hrana);
    }

    public Hrana dohvatiHranu(int idHrane){
        return hranaRepo.findById(idHrane).get();
    }

    public List<Hrana> dohvatiSvuHranu() {
        return hranaRepo.findAll();
    }
}
