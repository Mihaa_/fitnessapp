package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Hrana;
import ppij.FitnessApp.demo.Servisi.HranaServis;

import java.util.List;

@RestController
@RequestMapping("/hrana")
public class HranaKontroler {

    @Autowired
    HranaServis hranaServis;

    @PostMapping("/dodaj")
    public void dodajHranu(@RequestBody Hrana hrana){
        hranaServis.dodajHranu(hrana);
    }

    @GetMapping("/dohvati/{id}")
    public Hrana dohvatiHranu(@PathVariable(value="id") int idHrane){
        return hranaServis.dohvatiHranu(idHrane);
    }

    @GetMapping("/dohvati")
    public List<Hrana> dohvatiSvuHranu(){
        return hranaServis.dohvatiSvuHranu();
    }



}
