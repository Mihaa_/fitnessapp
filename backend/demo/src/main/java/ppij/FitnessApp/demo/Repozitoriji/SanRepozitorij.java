package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.San;

public interface SanRepozitorij extends JpaRepository<San, Integer> {
}
