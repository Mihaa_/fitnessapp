package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;
import ppij.FitnessApp.demo.Servisi.BodyMassIndexServis;

import java.util.Date;

@RestController
@RequestMapping("/bodyMassIndex")
public class BodyMassIndexKontroler {

    @Autowired
    BodyMassIndexServis bodyMassIndexServis;

    @PostMapping("/dodaj")
    public void dodajBodyMassIndex(@RequestBody BodyMassIndex bodyMassIndex){
        bodyMassIndexServis.dodajBodyMassIndex(bodyMassIndex);
    }

    @GetMapping("/dohvati/{id}")
    public BodyMassIndex dohvatiBodyMassIndex(@PathVariable(value="id") int idBodyMassIndex){
        return bodyMassIndexServis.dohvatiBodyMassIndex(idBodyMassIndex);
    }

    @GetMapping("/danas/{id}")
    public float izracunajBMI(@PathVariable(value="id") int idKor) {
        Date date = new Date();
        return bodyMassIndexServis.izracunajBMI(idKor, date);
    }
}
