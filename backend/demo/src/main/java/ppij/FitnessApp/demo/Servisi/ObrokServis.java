package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Obrok;
import ppij.FitnessApp.demo.Repozitoriji.HranaRepozitorij;
import ppij.FitnessApp.demo.Repozitoriji.ObrokRepozitorij;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class ObrokServis {

    @Autowired
    ObrokRepozitorij obRepo;
    @Autowired
    HranaRepozitorij hranaRepo;

    public void dodajObrok(Obrok obrok){
        obRepo.save(obrok);
    }

    public void obrisiObrok(Obrok obrok){
        obRepo.delete(obrok);
    }

    public Obrok dohvatiObrok(int idObroka){
        return obRepo.findById(idObroka).get();
    }

    public float kalorijeUDanu(int idKorisnika, Date datum) {
        List<Obrok> obroci = obRepo.findAll();
        float kalorije = 0;
        for (Obrok obrok : obroci) {
            if (obrok.getIdKor() == idKorisnika && obrok.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                kalorije += obrok.getKolicina() * hranaRepo.findById(obrok.getIdHrane()).get().getKalorije();
            }
        }
        return kalorije;
    }

    public List<Obrok> dohvatiMojeObroke(int idKorisnika){
        List<Obrok> sviObroci=obRepo.findAll();
        List<Obrok> mojiObroci=new LinkedList<>();

        for(Obrok obr:sviObroci){
            if(obr.getIdKor()==idKorisnika){
                mojiObroci.add(obr);
            }
        }
        return mojiObroci;
    }
}
