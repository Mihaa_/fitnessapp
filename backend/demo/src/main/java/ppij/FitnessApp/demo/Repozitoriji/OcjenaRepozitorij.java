package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Ocjena;

public interface OcjenaRepozitorij extends JpaRepository<Ocjena,Integer> {
}
