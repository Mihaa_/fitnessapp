package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Ocjena;
import ppij.FitnessApp.demo.Servisi.OcjenaServis;

@RestController
@RequestMapping("/ocjena")
public class OcjenaKontroler {

    @Autowired
    OcjenaServis ocjenaServis;

    @PostMapping("/dodaj")
    public void dodajOcjenu(@RequestBody Ocjena ocjena){
        ocjenaServis.dodajOcjenu(ocjena);
    }

    @GetMapping("/dohvati/{id}")
    public Ocjena dohvatiOcjenu(@PathVariable(value="id") int idOcjene){
        return ocjenaServis.dohvatiOcjenu(idOcjene);
    }



}
