package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Hrana;
import ppij.FitnessApp.demo.Modeli.Obrok;
import ppij.FitnessApp.demo.Modeli.ObrokHrana;
import ppij.FitnessApp.demo.Servisi.HranaServis;
import ppij.FitnessApp.demo.Servisi.ObrokServis;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/obrok")
public class ObrokKontroler {

    @Autowired
    ObrokServis obrokServis;
    @Autowired
    HranaServis hranaServis;

    @PostMapping("/dodaj")
    public void dodajObrok(@RequestBody Obrok obrok){
        obrokServis.dodajObrok(obrok);
    }

    @GetMapping("/dohvati/{id}")
    public Obrok dohvatiObrok(@PathVariable(value="id") int idObroka){
        return obrokServis.dohvatiObrok(idObroka);
    }

    @GetMapping("/dohvati/obrokHrana/{id}")
    public ObrokHrana dohvatiObrokHrana(@PathVariable(value="id") int idObroka){
        Obrok obr=dohvatiObrok(idObroka);
        Hrana hr=hranaServis.dohvatiHranu(obr.getIdHrane());
        return new ObrokHrana(obr,hr);
    }

    @GetMapping("/dohvati/mojiObroci/{id}")
    public List<Obrok> dohvatiMojeObroke(@PathVariable(value = "id")int idKorisnika){
        return obrokServis.dohvatiMojeObroke(idKorisnika);
    }

    @GetMapping("/danas/{id}")
    public float kalorijeUDanu(@PathVariable(value="id") int idKor) {
        Date date = new Date();
        return obrokServis.kalorijeUDanu(idKor, date);
    }
}
