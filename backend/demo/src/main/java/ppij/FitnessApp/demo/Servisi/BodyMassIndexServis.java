package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;
import ppij.FitnessApp.demo.Repozitoriji.BodyMassIndexRepozitorij;

import java.util.Collections;
import java.util.Date;
import java.util.List;


@Service
public class BodyMassIndexServis {

    @Autowired
    BodyMassIndexRepozitorij bodyMassIndexRepo;

    public void dodajBodyMassIndex(BodyMassIndex bodyMassIndex){
        bodyMassIndexRepo.save(bodyMassIndex);
    }

    public void obrisiBodyMassIndex(BodyMassIndex bodyMassIndex){
        bodyMassIndexRepo.delete(bodyMassIndex);
    }

    public BodyMassIndex dohvatiBodyMassIndex(int idBodyMassIndex){
        return bodyMassIndexRepo.findById(idBodyMassIndex).get();
    }

    public float izracunajBMI(int idKor, Date datum) {
        List<BodyMassIndex> bmievi = bodyMassIndexRepo.findAll();
        Collections.sort(bmievi);
        float bodyMassIndex = 0;
        for (BodyMassIndex bmi : bmievi) {
            if (bmi.getIdKor() == idKor && bmi.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                bodyMassIndex = bmi.getTezina()/(bmi.getVisina()*bmi.getVisina());
            }
        }
        return bodyMassIndex;
    }
}
