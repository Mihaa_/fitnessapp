package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Korisnik;
import ppij.FitnessApp.demo.Servisi.KorisnikServis;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/korisnik")
public class KorisnikKontroler {

    @Autowired
    KorisnikServis korisnikServis;

    @PostMapping("/dodaj")
    public void dodajKorisnika(@RequestBody Korisnik korisnik){
        korisnikServis.dodajKorisnika(korisnik);
    }

    @GetMapping("/dohvati/{id}")
    public Korisnik dohvatiKorisnika(@PathVariable(value="id") int idKorisnika){
        return korisnikServis.dohvatiKorisnika(idKorisnika);
    }

    @GetMapping("/tezine/{id}")
    public Map<String, Float> zadnjeTezine(@PathVariable(value="id") int idKorisnika){
        Date date = new Date();
        return korisnikServis.zadnjeTezine(idKorisnika, date);
    }

    @GetMapping("/snovi/{id}")
    public Map<String, Float> zadnjiSnovi(@PathVariable(value="id") int idKorisnika){
        Date date = new Date();
        return korisnikServis.zadnjiSnovi(idKorisnika, date);
    }

    @GetMapping("/dohvatiId/{email}")
    public int dohvatiId(@PathVariable(value="email") String email) {
        return korisnikServis.dohvatiKorisnikaPoEmailu(email).getId();
    }

}
