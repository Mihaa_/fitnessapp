package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;

@Entity
public class Hrana {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private String naziv;
    @Column(nullable = true)
    private float masti;
    @Column(nullable = true)
    private float protein;
    @Column(nullable = true)
    private float ugljikohidrati;
    @Column(nullable = true)
    private float kalorije;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public float getMasti() {
        return masti;
    }

    public void setMasti(float masti) {
        this.masti = masti;
    }

    public float getProtein() {
        return protein;
    }

    public void setProtein(float protein) {
        this.protein = protein;
    }

    public float getUgljikohidrati() {
        return ugljikohidrati;
    }

    public void setUgljikohidrati(float ugljikohidrati) {
        this.ugljikohidrati = ugljikohidrati;
    }

    public float getKalorije() {
        return kalorije;
    }

    public void setKalorije(float kalorije) {
        this.kalorije = kalorije;
    }
}
