package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Korisnik;

import java.util.Optional;

public interface KorisnikRepozitorij extends JpaRepository<Korisnik,Integer> {

    Korisnik findByEmail(String email);
    Korisnik findByIme(String ime);
    Boolean existsByEmail(String email);
    Boolean existsByIme(String ime);
}
