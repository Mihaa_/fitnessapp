package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;
import java.sql.Date;
import java.time.Instant;

@Entity
public class San implements Comparable<San> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private int idKor;
    @Column(nullable = true)
    private Date datum;
    @Column(nullable = true)
    private Instant pocetak;
    @Column(nullable = true)
    private Instant kraj;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKor() {
        return idKor;
    }

    public void setIdKor(int idKor) {
        this.idKor = idKor;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Instant getPocetak() {
        return pocetak;
    }

    public void setPocetak(Instant pocetak) {
        this.pocetak = pocetak;
    }

    public Instant getKraj() {
        return kraj;
    }

    public void setKraj(Instant kraj) {
        this.kraj = kraj;
    }

    @Override
    public int compareTo(San o) {
        return this.getDatum().compareTo(o.getDatum());
    }

}
