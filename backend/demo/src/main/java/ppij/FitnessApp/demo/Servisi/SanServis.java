package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.San;
import ppij.FitnessApp.demo.Repozitoriji.SanRepozitorij;

import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class SanServis {

    @Autowired
    SanRepozitorij sanRepo;

    public void dodajSan(San san){
        sanRepo.save(san);
    }

    public void obrisiSan(San san){
        sanRepo.delete(san);
    }

    public San dohvatiSan(int idSna){
        return sanRepo.findById(idSna).get();
    }

    public float snaUDanu(int idKor, Date datum) {
        List<San> snovi = sanRepo.findAll();
        Collections.sort(snovi);
        float vrijemeSna = 0;
        int vrijeme = 0;
        for (San san : snovi) {
            //System.out.println(san.getDatum().getTime()/1000000000 + "   " + datum.getTime()/1000000000);
            if (san.getIdKor() == idKor && san.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                vrijemeSna += Duration.between(san.getPocetak(), san.getKraj()).getSeconds();
                //System.out.print("aaaaaaaaaaa");
                //System.out.println(Duration.between(san.getPocetak(), san.getKraj()).getSeconds());
            }
        }
        return vrijemeSna/3600;
    }
}
