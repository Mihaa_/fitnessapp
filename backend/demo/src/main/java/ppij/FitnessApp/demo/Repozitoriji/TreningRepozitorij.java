package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Trening;

public interface TreningRepozitorij extends JpaRepository<Trening,Integer> {
}
