package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Trening;
import ppij.FitnessApp.demo.Modeli.TreningVjezba;
import ppij.FitnessApp.demo.Modeli.Vjezba;
import ppij.FitnessApp.demo.Servisi.TreningServis;
import ppij.FitnessApp.demo.Servisi.VjezbaServis;

import java.util.List;

@RestController
@RequestMapping("/trening")
public class TreningKontroler {

    @Autowired
    TreningServis treningServis;
    @Autowired
    VjezbaServis    vjezbaServis;

    @PostMapping("/dodaj")
    public void dodajTrening(@RequestBody Trening trening){
        treningServis.dodajTrening(trening);
    }

    @GetMapping("/dohvati/{id}")
    public Trening dohvatiTrening(@PathVariable(value="id") int idTreninga){
        return treningServis.dohvatiTrening(idTreninga);
    }

    @GetMapping("/dohvati/treningVjezba/{id}")
    public TreningVjezba dohvatiTreningVjezba(@PathVariable(value="id") int idTreninga){
        Trening tr=dohvatiTrening(idTreninga);
        Vjezba vj=vjezbaServis.dohvatiVjezbu(tr.getVjezba());
        return new TreningVjezba(tr,vj);
    }

    @GetMapping("/dohvatiMojeTreninge/{id}")
    public List<Trening> dohvatiMojeTreninge(@PathVariable(value = "id") int idKorisnika){
        return treningServis.dohvatiTreningeKorisnika(idKorisnika);
    }



}
