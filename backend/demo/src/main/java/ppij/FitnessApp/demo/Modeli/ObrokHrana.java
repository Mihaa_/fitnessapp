package ppij.FitnessApp.demo.Modeli;

public class ObrokHrana {
    private Obrok obrok;
    private Hrana hrana;

    public ObrokHrana(Obrok obrok, Hrana hrana) {
        this.obrok = obrok;
        this.hrana = hrana;
    }

    public Obrok getObrok() {
        return obrok;
    }

    public void setObrok(Obrok obrok) {
        this.obrok = obrok;
    }

    public Hrana getHrana() {
        return hrana;
    }

    public void setHrana(Hrana hrana) {
        this.hrana = hrana;
    }
}
