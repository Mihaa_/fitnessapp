package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Trening {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    @Column(nullable = true)
    private int idKor;
    @Column(nullable = true)
    private Date datum;
    @Column(nullable = true)
    private int vjezba;
    @Column(nullable = true)
    private float trajanje;

    public int getIdKor() {
        return idKor;
    }

    public void setIdKor(int idKor) {
        this.idKor = idKor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getVjezba() {
        return vjezba;
    }

    public void setVjezba(int vjezba) {
        this.vjezba = vjezba;
    }

    public float getTrajanje() {
        return trajanje;
    }

    public void setTrajanje(float trajanje) {
        this.trajanje = trajanje;
    }
}
