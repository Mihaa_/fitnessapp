package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Vjezba;
import ppij.FitnessApp.demo.Modeli.Vjezba;
import ppij.FitnessApp.demo.Repozitoriji.VjezbaRepozitorij;

import java.util.List;

@Service
public class VjezbaServis {

    @Autowired
    VjezbaRepozitorij vjezRepo;

    public void dodajVjezbu(Vjezba vjezba){
        vjezRepo.save(vjezba);
    }

    public void obrisiVjezbu(Vjezba vjezba){
        vjezRepo.delete(vjezba);
    }

    public Vjezba dohvatiVjezbu(int idVjezbe){
        return vjezRepo.findById(idVjezbe).get();
    }

    public List<Vjezba> dohvatiSveVjezbe() {
        return vjezRepo.findAll();
    }

}
