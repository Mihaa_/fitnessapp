package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;
import ppij.FitnessApp.demo.Servisi.BodyMassIndexServis;
import ppij.FitnessApp.demo.Servisi.DanasServis;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/danas")
public class DanasKontroler {

    @Autowired
    DanasServis danasServis;

    @GetMapping("/{id}")
    public List<Float> danas(@PathVariable(value="id") int idKor) {
        Date date = new Date();
        return danasServis.danas(idKor, date);
    }
}
