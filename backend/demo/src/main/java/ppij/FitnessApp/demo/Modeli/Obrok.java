package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Obrok {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private int idHrane;
    @Column(nullable = true)
    private int idKor;
    @Column(nullable = true)
    private Date datum;
    @Column(nullable = true)
    private float kolicina;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKor() {
        return idKor;
    }

    public void setIdKor(int idKor) {
        this.idKor = idKor;
    }

    public int getIdHrane() {
        return idHrane;
    }

    public void setIdHrane(int idHrane) {
        this.idHrane = idHrane;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public float getKolicina() {
        return kolicina;
    }

    public void setKolicina(float kolicina) {
        this.kolicina = kolicina;
    }
}
