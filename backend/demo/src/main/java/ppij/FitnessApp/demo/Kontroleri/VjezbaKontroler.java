package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.Hrana;
import ppij.FitnessApp.demo.Modeli.Vjezba;
import ppij.FitnessApp.demo.Servisi.VjezbaServis;

import java.util.List;

@RestController
@RequestMapping("/vjezba")
public class VjezbaKontroler {

    @Autowired
    VjezbaServis vjezbaServis;

    @PostMapping("/dodaj")
    public void dodajVjezbu(@RequestBody Vjezba vjezba){
        vjezbaServis.dodajVjezbu(vjezba);
    }

    @GetMapping("/dohvati/{id}")
    public Vjezba dohvatiVjezbu(@PathVariable(value="id") int idVjezbe){
        return vjezbaServis.dohvatiVjezbu(idVjezbe);
    }

    @GetMapping("/dohvati")
    public List<Vjezba> dohvatiSveVjezbe(){
        return vjezbaServis.dohvatiSveVjezbe();
    }




}
