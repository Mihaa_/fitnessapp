package ppij.FitnessApp.demo.Modeli;

public class TreningVjezba {
    private Trening trening;
    private Vjezba vjezba;

    public TreningVjezba(Trening trening, Vjezba vjezba) {
        this.trening = trening;
        this.vjezba = vjezba;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public Vjezba getVjezba() {
        return vjezba;
    }

    public void setVjezba(Vjezba vjezba) {
        this.vjezba = vjezba;
    }
}
