package ppij.FitnessApp.demo.Modeli;

public class MjestoProsjek implements Comparable<MjestoProsjek> {
    Mjesto mjesto;
    Float prosjek;

    public  MjestoProsjek(Mjesto mjesto,Float prosjek){
        this.mjesto=mjesto;
        this.prosjek=prosjek;
    }

    public Mjesto getMjesto() {
        return mjesto;
    }

    public void setMjesto(Mjesto mjesto) {
        this.mjesto = mjesto;
    }

    public Float getProsjek() {
        return prosjek;
    }

    public void setProsjek(Float prosjek) {
        this.prosjek = prosjek;
    }

    @Override
    public int compareTo(MjestoProsjek o) {
        return this.prosjek.compareTo(o.prosjek);
    }
}
