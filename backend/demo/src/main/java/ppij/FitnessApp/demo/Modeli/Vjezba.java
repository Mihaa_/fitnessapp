package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;

@Entity
public class Vjezba {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private String naziv;
    @Column(nullable = true)
    private float met;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public float getMet() {
        return met;
    }

    public void setMet(float met) {
        this.met = met;
    }
}
