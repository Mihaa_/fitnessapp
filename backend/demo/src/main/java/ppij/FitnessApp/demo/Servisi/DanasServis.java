package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;
import ppij.FitnessApp.demo.Modeli.Obrok;
import ppij.FitnessApp.demo.Modeli.San;
import ppij.FitnessApp.demo.Modeli.Trening;
import ppij.FitnessApp.demo.Repozitoriji.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class DanasServis {

    @Autowired
    KorisnikRepozitorij korRepo;
    @Autowired
    BodyMassIndexRepozitorij bodyMassIndexRepo;
    @Autowired
    ObrokRepozitorij obRepo;
    @Autowired
    HranaRepozitorij hranaRepo;
    @Autowired
    SanRepozitorij sanRepo;
    @Autowired
    TreningRepozitorij trenRepo;
    @Autowired
    VjezbaRepozitorij vjezRepo;


    public List<Float> danas(int idKorisnika, Date datum) {
        List<Obrok> obroci = obRepo.findAll();
        float uneseneKalorije = 0;
        for (Obrok obrok : obroci) {
            if (obrok.getIdKor() == idKorisnika && obrok.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                uneseneKalorije += obrok.getKolicina() * hranaRepo.findById(obrok.getIdHrane()).get().getKalorije();
            }
        }
        List<BodyMassIndex> bmievi = bodyMassIndexRepo.findAll();
        float bodyMassIndex = 0;
        for (BodyMassIndex bmi : bmievi) {
            if (bmi.getIdKor() == idKorisnika && bmi.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                bodyMassIndex = bmi.getTezina()/(bmi.getVisina()*bmi.getVisina());
            }
        }
        List<San> snovi = sanRepo.findAll();
        float vrijemeSna = 0;
        for (San san : snovi) {
            if (san.getIdKor() == idKorisnika && san.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                vrijemeSna += Duration.between(san.getPocetak(), san.getKraj()).getSeconds();
            }
        }
        List<Trening> treninzi = trenRepo.findAll();
        float potroseneKalorije = 0;
        for (Trening trening : treninzi) {
            if (trening.getIdKor() == idKorisnika && trening.getDatum().getTime()/1000000000 == datum.getTime()/1000000000) {
                //potroseneKalorije += vjezRepo.findById(trening.getVjezba()).get().getMet() * trening.getTrajanje() *
                 //korRepo.findById(trening.getIdKor()).get().getTezina();
                //System.out.println(potroseneKalorije);
                potroseneKalorije += vjezRepo.findById(trening.getVjezba()).get().getMet() * trening.getTrajanje() *
                        korRepo.findById(trening.getIdKor()).get().getTezina();
            }
        }
        //float kalorije = uneseneKalorije - potroseneKalorije;
        List<Float> rezultati = new ArrayList<>();
        rezultati.add(bodyMassIndex);
        rezultati.add(uneseneKalorije);
        rezultati.add(potroseneKalorije);
        rezultati.add(vrijemeSna/60);
        return rezultati;
    }
}
