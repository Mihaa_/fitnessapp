package ppij.FitnessApp.demo.Kontroleri;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ppij.FitnessApp.demo.Modeli.San;
import ppij.FitnessApp.demo.Servisi.SanServis;

import java.util.Date;

@RestController
@RequestMapping("/san")
public class SanKontroler {

    @Autowired
    SanServis sanServis;

    @PostMapping("/dodaj")
    public void dodajSan(@RequestBody San san){
        sanServis.dodajSan(san);
    }

    @GetMapping("/dohvati/{id}")
    public San dohvatiSan(@PathVariable(value="id") int idSna){
        return sanServis.dohvatiSan(idSna);
    }

    @GetMapping("/danas/{id}")
    public float izracunajBMI(@PathVariable(value="id") int idKor) {
        Date date = new Date();
        return sanServis.snaUDanu(idKor, date);
    }
}
