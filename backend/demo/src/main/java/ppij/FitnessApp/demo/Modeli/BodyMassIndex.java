package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;
import java.util.Date;

@Entity
public class BodyMassIndex implements Comparable<BodyMassIndex> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private int idKor;
    @Column(nullable = true)
    private Date datum;
    @Column(nullable = true)
    private float tezina;
    @Column(nullable = true)
    private float visina;
    @Column(nullable = true)
    private float bmi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKor() {
        return idKor;
    }

    public void setIdKor(int idKor) {
        this.idKor = idKor;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public float getTezina() {
        return tezina;
    }

    public void setTezina(float tezina) {
        this.tezina = tezina;
    }

    public float getVisina() {
        return visina;
    }

    public void setVisina(float visina) {
        this.visina = visina;
    }

    public float getBmi() {
        return bmi;
    }

    public void setBmi(float bmi) {
        this.bmi = bmi;
    }

    @Override
    public int compareTo(BodyMassIndex o) {
        return this.getDatum().compareTo(o.getDatum());
    }
}
