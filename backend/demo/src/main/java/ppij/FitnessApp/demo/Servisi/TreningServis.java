package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Korisnik;
import ppij.FitnessApp.demo.Modeli.Trening;
import ppij.FitnessApp.demo.Repozitoriji.TreningRepozitorij;

import java.util.LinkedList;
import java.util.List;

@Service
public class TreningServis {

    @Autowired
    TreningRepozitorij trenRepo;

    public void dodajTrening(Trening trening) { trenRepo.save(trening); }

    public void obrisiTrening(Trening trening) { trenRepo.delete(trening); }

    public Trening dohvatiTrening(int idTreninga){
        return trenRepo.findById(idTreninga).get();
    }

    public List<Trening> dohvatiTreningeKorisnika(int idKorisnika){
        List<Trening> pomocna=trenRepo.findAll();
        List<Trening> mojiTren=new LinkedList<>();

        for(Trening tr: pomocna){
            if(tr.getIdKor()==idKorisnika){
                mojiTren.add(tr);
            }
        }
        return mojiTren;
    }
}
