package ppij.FitnessApp.demo.Modeli;


import javax.persistence.*;

@Entity
public class Ocjena {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = true)
    private int idKor;
    @Column(nullable = true)
    private int idMjesta;
    @Column(nullable = true)
    private int ocjena;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKor() {
        return idKor;
    }

    public void setIdKor(int idKor) {
        this.idKor = idKor;
    }

    public int getIdMjesta() {
        return idMjesta;
    }

    public void setIdMjesta(int idMjesta) {
        this.idMjesta = idMjesta;
    }

    public int getOcjena() {
        return ocjena;
    }

    public void setOcjena(int ocjena) {
        this.ocjena = ocjena;
    }
}
