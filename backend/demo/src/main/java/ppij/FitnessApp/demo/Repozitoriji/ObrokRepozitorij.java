package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.Obrok;

public interface ObrokRepozitorij extends JpaRepository<Obrok,Integer> {
}
