package ppij.FitnessApp.demo.Kontroleri;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ppij.FitnessApp.demo.Modeli.Korisnik;
import ppij.FitnessApp.demo.Repozitoriji.KorisnikRepozitorij;

@RestController
@RequestMapping("/users")
public class UserController {


    private KorisnikRepozitorij korisnikRepo;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(KorisnikRepozitorij applicationUserRepository,BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.korisnikRepo = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody Korisnik user) {
        if (user.getPassword().equals(user.getConfirmPassword())) {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            korisnikRepo.save(user);
        } else {
            System.out.println("no!");
        }
    }
}
