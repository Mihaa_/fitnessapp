package ppij.FitnessApp.demo.Repozitoriji;

import org.springframework.data.jpa.repository.JpaRepository;
import ppij.FitnessApp.demo.Modeli.BodyMassIndex;

public interface BodyMassIndexRepozitorij extends JpaRepository<BodyMassIndex, Integer> {
}
