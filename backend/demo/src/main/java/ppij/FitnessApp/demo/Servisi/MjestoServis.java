package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Mjesto;
import ppij.FitnessApp.demo.Modeli.MjestoProsjek;
import ppij.FitnessApp.demo.Modeli.Ocjena;
import ppij.FitnessApp.demo.Repozitoriji.MjestoRepozitorij;
import ppij.FitnessApp.demo.Repozitoriji.OcjenaRepozitorij;

import java.util.*;

@Service
public class MjestoServis {

    @Autowired
    MjestoRepozitorij mjRepo;
    @Autowired
    OcjenaRepozitorij ocjRepo;
    public void dodajMjesto(Mjesto mjesto){
        mjRepo.save(mjesto);
    }

    public void obrisiMjesto(Mjesto mjesto){
        mjRepo.delete(mjesto);
    }

    public Mjesto dohvatiMjesto(int idMjesta){
        return mjRepo.findById(idMjesta).get();
    }

    public float prosjecna_ocjena(int idMjesta) {
        List<Ocjena> ocjene = ocjRepo.findAll();
        int suma = 0;
        int i = 0;
        for (Ocjena ocjena : ocjene) {
            //System.out.println(san.getDatum().getTime()/1000000000 + "   " + datum.getTime()/1000000000);
            if (ocjena.getIdMjesta() == idMjesta) {
                suma += ocjena.getOcjena();
                i++;
                //System.out.print("aaaaaaaaaaa");
                //System.out.println(Duration.between(san.getPocetak(), san.getKraj()).getSeconds());
            }
        }
        if (i == 0) return 0;
        else return (float) suma / i;
    }

    public List<MjestoProsjek> najboljihPet() {
        List<Mjesto> mjesta = mjRepo.findAll();
        List<MjestoProsjek> najbolji = new ArrayList<>();

        for(Mjesto mjesto:mjesta){
            najbolji.add(new MjestoProsjek(mjesto,prosjecna_ocjena(mjesto.getId())));
        }
        Collections.sort(najbolji);
        return najbolji;

    }

    public List<Mjesto> dohvatiSvaMjesta() {
        return mjRepo.findAll();
    }
}
