package ppij.FitnessApp.demo.Servisi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppij.FitnessApp.demo.Modeli.Ocjena;
import ppij.FitnessApp.demo.Repozitoriji.OcjenaRepozitorij;

@Service
public class OcjenaServis {

    @Autowired
    OcjenaRepozitorij ocjRepo;

    public void dodajOcjenu(Ocjena ocjena){
        {
            if (ocjena.getOcjena() >= 1 && ocjena.getOcjena() <=5) {
                ocjRepo.save(ocjena);
            }
        }
    }

    public void obrisiOcjenu(Ocjena ocjena){
        ocjRepo.delete(ocjena);
    }

    public Ocjena dohvatiOcjenu(int idOcjene){
        return ocjRepo.findById(idOcjene).get();
    }


}
